module.exports = {
    extends: ['tslint-config-standard'],
    rules: {
        'ter-indent': [
            true,
            4,
            {
                SwitchCase: 1
            }
        ],
        'class-name': true,
        'comment-format': [ false ],
    }
}
