# TSLint Config Terreon

[![NPM version](https://img.shields.io/npm/v/tslint-config-terreon.svg?style=flat)](https://npmjs.org/package/tslint-config-terreon)
[![NPM downloads](https://img.shields.io/npm/dm/tslint-config-terreon.svg?style=flat)](https://npmjs.org/package/tslint-config-terreon)

> A [TSLint config](https://palantir.github.io/tslint/usage/tslint-json/) based on [JavaScript Standard Style](http://standardjs.com/) with some changes:
>
> - **4 spaces** – for indentation 
> - **No space after function name** – `function name(arg) { ... }`
> - **No spaces required inside comments** – `//console.log('foo')`

## Installation

```sh
npm install tslint-config-terreon --save-dev
```

## Usage

In `tslint.json`:

```json
{
  "extends": "tslint-config-terreon"
}
```

**P.S.** Some TSLint rules may require the use of `--project`.

### Rules

* [TSLint](https://www.npmjs.com/package/tslint)
* [TSLint ESLint Rules](https://www.npmjs.com/package/tslint-eslint-rules)
* [TSLint Config Standard](https://www.npmjs.com/package/tslint-config-standard)

## License

Apache 2.0
